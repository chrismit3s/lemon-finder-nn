@echo off

call %~dp0\..\venv\Scripts\activate.bat
tensorboard --logdir %~dp0\..\logs --port 1337 --bind_all
