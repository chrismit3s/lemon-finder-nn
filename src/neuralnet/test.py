from argparse import ArgumentParser
from os.path import join
from src.dataset import all_classes, data_format
from src.dataset.augment import ratiocrop_image, resize_image
from src.helpers import remove_channels
from src.neuralnet.model import load_model
from src.neuralnet import batch_size, model_path
import numpy as np
import cv2
import os


def get_pred_str(labels, pred):
    m = np.argmax(pred)
    ret = ""
    for i, (label, value) in enumerate(zip(labels, pred)):
        if i == m:
            ret += f"[{label}={value:4.2f}]"
        else:
            ret += f" {label}={value:4.2f} "
    return ret


def main(model, img_dir, show=False):
    # tfs image data generator sorts labels alphabetically
    labels = sorted(all_classes)

    # get the input size
    input_shape = model.layers[0].input_shape
    input_shape = input_shape[0] if len(input_shape) == 1 else input_shape
    input_size = remove_channels(input_shape[1::], data_format)  # 1:: to skip the batch dim

    print("loading images...", end="\r", flush=True)
    rgb_imgs = []
    bgr_imgs = []
    names = sorted(os.listdir(img_dir))
    for img_name in names:
        # crop image to right size
        img = cv2.imread(join(img_dir, img_name))
        img = ratiocrop_image(img, input_size[0] / input_size[1])
        img = resize_image(img, size=input_size, interp="cubic")
        bgr_imgs.append(img)

        # tfs image data generator output in rgb, opencv in bgr
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        rgb_imgs.append(img)
    imgs = np.array(rgb_imgs)
    print("loading images - done")

    print("predicting...", end="\r", flush=True)
    if len(imgs) > batch_size and False:
        preds = model.predict(imgs, batch_size=batch_size)
    else:
        preds = model(imgs, training=False)
    print("predicting - done")

    max_name_len = max(len(img_name) for img_name in names)
    for img_name, img, pred in zip(names, bgr_imgs, preds):
        print(f"{img_name + ':':{max_name_len + 1}} {get_pred_str(labels, pred)}")
        if show:
            cv2.imshow("Model input", resize_image(img, scale=5, interp="nearest"))
            cv2.waitKey(0)


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-m", "--model",
                        dest="model_path",
                        help="path to the model to test",
                        type=str,
                        default=join(model_path, os.listdir(model_path)[-1]))
    parser.add_argument("-i", "--image-dir",
                        required=True,
                        dest="img_dir",
                        help=("directory containing the images to dest on, they "
                              "will be resized and cropped if needed"),
                        type=str)
    parser.add_argument("-s", "--show",
                        dest="show",
                        help=("whether or not to show the image like it will be "
                              "passed to the neuralnet in a seperate window"),
                        action="store_true")

    args = parser.parse_args()

    m = load_model(args.model_path)
    main(m, args.img_dir, args.show)
