from argparse import ArgumentParser
from os.path import join
from src.neuralnet import model_path
from src.neuralnet.model import load_model
import os
import tensorflow as tf


def save_as_tflite(model, filename, use_float16=False):
    """
    takes a keras model `model` and writes it to `filename` as a TFLite model
    """

    converter = tf.lite.TFLiteConverter.from_keras_model(model)

    if use_float16:
        converter.optimizations = [tf.lite.Optimize.DEFAULT]
        converter.target_spec.supported_types = [tf.float16]

    with open(filename, "wb") as file:
        file.write(converter.convert())


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-m", "--model",
                        dest="model_path",
                        help="path to the model to convert to tflite",
                        type=str,
                        default=join(model_path, os.listdir(model_path)[-1]))
    parser.add_argument("-o", "--out",
                        dest="out_path",
                        help="name of the file for the converted model",
                        type=str,
                        default=None)
    parser.add_argument("--float16",
                        dest="use_float16",
                        help="apply float16 quantization to reduce model size",
                        action="store_true")

    args = parser.parse_args()

    # create output filename
    if args.out_path is None:
        if args.model_path.endswith(".h5"):
            args.out_path = args.model_path[:-3] + ".tflite"
        else:
            args.out_path = args.model_path + ".tflite"

    # load model to convert
    print("loading model...", end="\r", flush=True)
    m = load_model(args.model_path)
    print("loading model - done")

    # convert model
    print("converting model...", end="\r", flush=True)
    save_as_tflite(m, args.out_path, args.use_float16)
    print("converting model - done")
