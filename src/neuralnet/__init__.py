from os.path import join
from keras.optimizers import Adam


log_path = join(".", "logs")
model_path = join(".", "models")

optimizer = Adam(learning_rate=1e-3)
loss = "categorical_crossentropy"
metrics = ["acc"]

epochs = 128
batch_size = 64
patience = 32
