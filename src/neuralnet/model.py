from argparse import ArgumentParser
from keras.layers import Activation, BatchNormalization, GaussianNoise, Input  # noqa: F401
from keras.layers import Conv2D, Dense, Flatten, MaxPooling2D  # noqa: F401
from keras.layers.experimental.preprocessing import Rescaling  # noqa: F401
from keras.models import Sequential, Model, load_model  # noqa: F401
from keras.applications import MobileNetV2  # noqa: F401
from src.dataset import classes, data_format, input_shape  # noqa: F401
from src.neuralnet import loss, metrics, optimizer


def create_model(optimizer=optimizer, loss=loss, metrics=metrics):
    """
    returns a compiled sequential model ready for training; `optimizer`, `loss`
    and `metrics` are passed to
    [`keras.models.Sequential.compile`](https://keras.io/api/models/model_training_apis/#compile-method)
    """

    # setup common args for layer types
    activation = "relu"  # "swish"

    pretrained = MobileNetV2(weights="imagenet",
                             input_shape=input_shape,
                             include_top=False)
    pretrained.trainable = False

    # start model
    x = inputs = Input(shape=input_shape)

    # maps 0..1 (dataset) to -1..1 (mobilenet/nasnet input)
    x = Rescaling(scale=2, offset=-1)(x)
    x = pretrained(x, training=False)
    x = Flatten()(x)

    x = Dense(units=128, activation=activation)(x)
    x = Dense(units=128, activation=activation)(x)
    x = Dense(units=128, activation=activation)(x)

    # final/output layer, +1 for nomatch class
    outputs = Dense(units=len(classes) + 1, activation="softmax")(x)

    m = Model(inputs, outputs)
    m.compile(optimizer=optimizer,
              loss=loss,
              metrics=metrics)
    return m


def summarize(model):
    model.summary(line_length=120,
                  print_fn=lambda s, *a, **kw: None if set(s) == {"_"} else print(s, *a, **kw))


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-m", "--model",
                        dest="model_filename",
                        help="path to the model to summarize",
                        type=str,
                        default=None)

    args = parser.parse_args()

    if args.model_filename is None:
        print("building model...", end="\r", flush=True)
        m = create_model()
        print("building model - done")
    else:
        print("loading model...", end="\r", flush=True)
        m = load_model(args.model_filename)
        print("loading model - done")
    summarize(m)
