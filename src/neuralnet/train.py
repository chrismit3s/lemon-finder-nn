from argparse import ArgumentParser
from contextlib import suppress
from keras.callbacks import TensorBoard, EarlyStopping, ModelCheckpoint
from os.path import join
from src.helpers import isotime
from src.dataset import load_data
from src.neuralnet import log_path, model_path, epochs, patience, batch_size
from src.neuralnet.model import create_model, load_model, summarize
import os
import shutil as sh


def train(model, train_data, val_data, log_dir, checkpoint_filename, patience=patience, epochs=epochs, **kwargs):
    """
    trains `model` on `train_data` and tests it on `val_data`, tensorboard logs
    are stored in `log_dir`, the best model checkpoint is stored in
    `checkpoint_filename`, other keyword arguments are directly passed on to
    [`keras.Model.fit`](https://keras.io/api/models/model_training_apis/#fit-method),
    usually you'd want to at least pass `epochs` and `batch_size`
    """

    tensorboard = TensorBoard(log_dir=log_dir,
                              histogram_freq=2)
    stopping = EarlyStopping(monitor="val_loss",
                             patience=patience,
                             min_delta=0,
                             verbose=1,
                             mode="min")
    checkpoint = ModelCheckpoint(monitor="val_acc",
                                 verbose=1,
                                 mode="max",
                                 save_best_only=True,
                                 save_weights_only=False,
                                 filepath=checkpoint_filename)

    return model.fit(x=train_data,
                     epochs=epochs,
                     callbacks=[tensorboard, stopping, checkpoint],
                     shuffle=True,
                     validation_data=val_data,
                     verbose=1,
                     **kwargs)


if __name__ == "__main__":
    parser = ArgumentParser()

    model_src_group = parser.add_mutually_exclusive_group()

    model_src_group.add_argument("-m", "--model",
                                 dest="model_filename",
                                 help="path to the model to train",
                                 type=str,
                                 default=None)

    model_src_group.add_argument("-c", "--continue",
                                 dest="model_filename",
                                 help="continue training the last model",
                                 action="store_const",
                                 const=join(model_path, os.listdir(model_path)[-1]))

    args = parser.parse_args()

    model_filename = args.model_filename
    timestr = isotime()

    print("Model summary:")
    if model_filename is None:
        # create new model
        print("building model...", end="\r", flush=True)
        m = create_model()
        print("building model - done")
    else:
        # load an old model
        print("loading model...", end="\r", flush=True)
        m = load_model(model_filename)
        print("loading model - done")
    summarize(m)
    print("")

    print("Training model:")
    print("loading dataset...", end="\r", flush=True)
    train_gen, val_gen = load_data()
    print(f"loading dataset - done - {train_gen.n} training images, {val_gen.n} validation images")

    print("starting training" if model_filename is None else f"continuing training for {model_filename}")
    log_dir = join(log_path, f"model-{timestr}")
    model_filename = join(model_path, f"model-{timestr}-end.h5")
    checkpoint_filename = join(model_path, f"model-{timestr}-best.h5")
    save = True
    try:
        hist = train(m,
                     log_dir=log_dir,
                     checkpoint_filename=checkpoint_filename,
                     batch_size=batch_size,
                     train_data=train_gen,
                     val_data=val_gen)
    except KeyboardInterrupt:
        print("")
        save = input("training interrupted, save model? (Y/n) ").lower()[:1] != "n"
    except Exception as err:
        print("")
        save = input(f"training exited with exception {err}, save model? (Y/n) ").lower()[:1] != "n"

    if save:
        # save end-model and keep logs and model checkpoint
        print("saving model...", end="\r", flush=True)
        m.save(model_filename)
        print(f"saving model - done - {model_filename}")
    else:
        # discard model and delete logs and model checkpoint
        print(f"{log_dir=}")
        print("cleaning logs...", end="\r", flush=True)
        sh.rmtree(log_dir)
        with suppress(FileNotFoundError):
            os.remove(checkpoint_filename)
        print("cleaning logs - done")
