from contextlib import redirect_stderr
from hashlib import sha256
from mimetypes import guess_extension
from time import strftime
import cv2
import numpy as np
import os
import requests as rq


def rprint(*out_strs, sep=" ", end="\n", **kwargs):
    """
    just like the `print` builtin, but makes sure the previous line is
    completely overwritten when using a carriage return as `end`
    """
    print_str = sep.join(str(s) for s in out_strs)
    to_fill = getattr(rprint, "_len_last_print", 0) - len(print_str)
    print(print_str + " " * to_fill, end=end, **kwargs)
    rprint._len_last_print = len(print_str.rstrip()) if end[0] == "\r" else 0


def int_tuple(a):
    return tuple(int(x) for x in a)


def lookup_attr(obj, prefix, attr, toupper=False, forward_error=True):
    """
    looks up, whether the object `obj` has the attribute `prefix + attr` (default)
    or `prefix + attr.upper()` (if `toupper` is `True`), if it doesn't it'll raise
    `AttributeError` with a helpful error message, containing the original error
    if `forward_error` is `True`
    """

    try:
        return getattr(obj, prefix + attr.upper() if toupper else attr)
    except AttributeError as err:
        # get list of possible attributes starting with prefix
        skip = len(prefix)
        known = [a[skip:] for a in dir(obj) if a.startswith(prefix)]

        # get list of a few example attributes (to avoid cluttering the error message)
        known_str = ", ".join(known[:5])
        if len(known) > 5:
            known_str += ", ..."

        # if toupper is set, the actual case of attr is ignored, so it might as
        # well be all lowercase for aesthetics
        if toupper:
            known_str = known_str.lower()

        # build error message
        err = f"Unknown attribute {attr!r}, known attributes are {known_str}"
        if forward_error:
            err += f" (original error was {err!r})"

        raise AttributeError(err)


def remove_channels(shape, data_format):
    """
    takes a frame shape `shape` and returns its size as a tuple (width, height)
    _without_ channel information
    """
    if data_format == "channels_last":
        return shape[:-1][::-1]  # numpy indexes images as y,x
    elif data_format == "channels_first":
        return shape[2:][::-1]
    else:
        raise ValueError(f"Unknown data format {data_format!r}")


def add_channels(size, data_format):
    """
    takes a frame size `size` and returns its shape as a numpy shape tuple
    """
    if data_format == "channels_last":
        return (*size[::-1], 3)  # numpy indexes images as y,x
    elif data_format == "channels_first":
        return (3, *size[::-1])
    else:
        raise ValueError(f"Unknown data format {data_format!r}")


def write_images(img_gen, filename_fmt):
    """
    writes the ith image in `img_gen` as `filename_fmt.format(i)`
    """
    for i, img in enumerate(img_gen):
        cv2.imwrite(filename_fmt.format(i), img)
        yield i


def download_file(url, filename, noext=False):
    """
    downloads a file from `url` to the file `filename`, if `noext` is `True`,
    the `content-type` extension is appended to `filename` seperated by a dot
    (`.`) and that modified filename is returned
    """

    # request file
    r = rq.get(url, allow_redirects=False)

    # add extension
    if noext:
        content_type = r.headers.get("content-type").partition(";")[0].strip()
        filename += guess_extension(content_type)

    # write binary data
    with open(filename, "wb+") as f:
        f.write(r.content)

    return filename


def image_from_url(url, max_retries=1, **kwargs):
    """
    downloads an image from `url` and returns it as a numpy array, it will retry
    `max_retries` times before giving up and raising `ConnectionError` if
    something went wrong every single time, other keyword arguements are passed on
    to [`requests.get`](https://requests.readthedocs.io/en/latest/api/#requests.get)
    """

    errors = []
    for _ in range(max_retries):
        # sometimes opencv outputs some warnings about corrupt jpeg info, etc.
        # and returns None, we check for None but don't want that output
        try:
            # request data and check status code
            resp = rq.get(url, **kwargs)
            resp.raise_for_status()

            # decode image
            img = cv2.imdecode(np.frombuffer(resp.content, "uint8"),
                               flags=cv2.IMREAD_COLOR)
        except cv2.error:
            errors.append("decoding failed")
            continue
        except rq.exceptions.RequestException:
            errors.append("download failed")
            continue

        # check if image data was corrupt
        if img is None:
            errors.append("no image data")
            continue

        # we made it through all error checks, the image data is actually valid
        break  # skips the else clause
    else:
        raise ConnectionError(f"Failed to download image from {url} after "
                              f"retrying {max_retries} times, errors where: "
                              f"{', '.join(errors)}")

    return img


def hash_directory(path):
    """
    sha256-hashes all files and subdirs in a directory, returns a hash object
    """

    # adapted from https://stackoverflow.com/q/24937495/
    hash = sha256()
    for root, dirs, files in os.walk(path):
        for name in files:
            filepath = os.path.join(root, name)
            # print("hashing", filepath)
            hash.update(filepath[len(path):].encode())

            if not os.path.isfile(filepath):
                continue

            with open(filepath, 'rb') as f:
                for chunk in iter(lambda: f.read(1024 * 1024), b""):
                    hash.update(chunk)
    return hash


def isotime():
    """
    returns the current time formatted in ISO8601-ish, the time seperators are
    `'-'` instead of `':'` so that it can be used in a filename
    """

    return strftime('%Y-%m-%dT%H-%M-%S')
