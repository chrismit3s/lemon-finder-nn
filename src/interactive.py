from contextlib import suppress
from math import ceil
from src.dataset import data_format, image_size, imagenet_dataset
from src.dataset.augment import ratiocrop_image, resize_image
from src.helpers import image_from_url, remove_channels  # noqa: F401
from nltk.corpus import wordnet as wn
import cv2  # noqa: F401
import numpy as np  # noqa: F401
import requests as rq  # noqa: F401


def show_img(img, title="Preview", zoom=1, delay=1):
    cv2.imshow(title, resize_image(img, scale=zoom))
    cv2.waitKey(delay)


def add_noise(img, stddev):
    noise = np.random.normal(0, stddev, size=img.shape)
    return np.clip(img + 255 * noise, 0, 255, dtype="uint8")


def show_img_grid(imgs, rows=1, cols=None, img_size=None, title="Preview", **kwargs):
    # check arguements
    if rows is cols is None:
        raise ValueError("Only one of rows, cols can be None, not both")

    n = len(imgs)
    rows = rows or ceil(n / cols)
    cols = cols or ceil(n / rows)
    img_size = img_size or remove_channels(imgs[0].shape, data_format)

    ratio = img_size[0] / img_size[1]
    for i, img in enumerate(imgs):
        img = ratiocrop_image(img, ratio=ratio)
        img = resize_image(img, size=img_size)
        imgs[i] = img
    imgs += (rows * cols - n) * [np.zeros(shape=imgs[0].shape, dtype=imgs[0].dtype)]

    # fill grid
    grid = None
    for r in range(rows):
        row = np.hstack(imgs[r * cols:(r + 1) * cols])
        grid = row if grid is None else np.vstack((grid, row))

    if img_size is not None:
        grid_size = np.array(img_size) * (rows, cols)
        grid = resize_image(grid, size=grid_size)

    show_img(grid, title=title, **kwargs)


def preview_synset(wnid, n=25, **kwargs):
    resp = rq.get(imagenet_dataset["urls"].format(wnid))
    urls = [line.strip() for line in resp.text.split("\n")]

    imgs = []
    for url in urls:
        with suppress(ConnectionError):
            if (img := image_from_url(url, timeout=2)) is not None:
                imgs.append(img)
        if len(imgs) == n:
            break

    show_img_grid(imgs, rows=ceil(n ** 0.5), title=f"Synset {wnid}", **kwargs)


def wnids_for(noun):
    return ["n{:08}".format(s.offset()) for s in wn.synsets(noun)]
