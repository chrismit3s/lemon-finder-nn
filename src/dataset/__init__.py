from contextlib import redirect_stdout
from keras.preprocessing.image import ImageDataGenerator
from os.path import join
from src.helpers import add_channels
from src.neuralnet import batch_size
import numpy as np
import os
import random


random_seed = None  # int.from_bytes(b"2019-10-23", byteorder="little") % (2 ** 31 - 1)

request_timeout = 3  # seconds
max_request_retries = 3

data_path = join(".", "data")

classes = ["apple", "banana", "lemon"]
no_match_class = "none-of-the-above"
all_classes = classes + [no_match_class]

image_size = (128, 128)
data_format = "channels_last"
input_shape = add_channels(image_size, data_format)

video_dataset = {
        "path": join(".", "raw", "videos"),
        "augment-factor": 1,

        # 0.15 means the at first the area of the image will be made 15% too big
        # along each edge, so 30% in total, and then it will be cropped down
        # randomly to the actual input size
        "min-crop-border": 0,

        "frame-skip": 4}

kaggle_dataset = {
        "path": join(".", "raw", "fruits-360"),
        "name": "moltean/fruits",
        "augment-factor": 1,

        "classes": {
            "apple": [
                "Apple Braeburn",
                "Apple Crimson Snow",

                "Apple Golden 1",
                "Apple Golden 2",

                # use some images of a green apples twice, to combat the bias against green apples
                "Apple Golden 3",
                "Apple Granny Smith",

                "Apple Pink Lady",

                "Apple Red 1",
                "Apple Red 2",
                "Apple Red 3",

                "Apple Red Delicious",
                "Apple Red Yellow 1",
                "Apple Red Yellow 2"],
            "banana": [
                "Banana",
                "Banana",
                "Banana",
                "Banana",
                "Banana Lady Finger",
                "Banana Lady Finger"],
            "lemon": [
                "Lemon",
                "Lemon",
                "Lemon",
                "Lemon",
                "Limes",
                "Limes"],
            no_match_class: [  # all other fruits
                "Apricot", "Avocado", "Avocado ripe",  # "Beetroot", "Blueberry",
                # "Cactus fruit", "Cantaloupe 1", "Cantaloupe 2", "Carambula",
                # "Cauliflower", "Cherry 1", "Cherry 2", "Cherry Rainier",
                # "Cherry Wax Black", "Cherry Wax Red", "Cherry Wax Yellow",
                # "Chestnut", "Clementine", "Cocos", "Corn", "Corn Husk",
                # "Cucumber Ripe", "Cucumber Ripe 2", "Dates", "Eggplant", "Fig",
                # "Ginger Root", "Granadilla", "Grape Blue", "Grape Pink",
                # "Grape White", "Grape White 2", "Grape White 3", "Grape White 4",
                # "Grapefruit Pink", "Grapefruit White", "Guava", "Hazelnut",
                # "Huckleberry", "Kaki", "Kiwi", "Kohlrabi", "Kumquats", "Lychee",
                # "Mandarine", "Mango", "Mango Red", "Mangostan", "Maracuja",
                # "Melon Piel de Sapo", "Mulberry", "Nectarine", "Nectarine Flat",
                # "Nut Forest", "Nut Pecan", "Onion Red", "Onion Red Peeled",
                # "Onion White", "Orange", "Papaya", "Passion Fruit", "Peach",
                # "Peach 2", "Peach Flat", "Pear", "Pear 2", "Pear Abate",
                # "Pear Forelle", "Pear Kaiser", "Pear Monster", "Pear Red",
                # "Pear Stone", "Pear Williams", "Pepino", "Pepper Green",
                # "Pepper Orange", "Pepper Red", "Pepper Yellow", "Physalis",
                # "Physalis with Husk", "Pineapple", "Pineapple Mini", "Pitahaya Red",
                # "Plum", "Plum 2", "Plum 3", "Pomegranate", "Pomelo Sweetie",
                "Potato Red", "Potato Red Washed", "Potato Sweet", "Potato White",
                # "Quince", "Rambutan", "Raspberry", "Redcurrant", "Salak", "Strawberry",
                "Strawberry Wedge", "Tamarillo", "Tangelo", "Tomato 1", "Tomato 2",
                "Tomato 3", "Tomato 4", "Tomato Cherry Red", "Tomato Heart",
                # "Tomato Maroon", "Tomato not Ripened", "Tomato Yellow", "Walnut",
                " Watermelon"]}}

imagenet_dataset = {
        "augment-factor": 3,

        # 0.15 means the at first the area of the image will be made 15% too big
        # along each edge, so 30% in total, and then it will be cropped down
        # randomly to the actual input size
        "min-crop-border": 0.15,

        "all-synsets": "http://www.image-net.org/api/text/imagenet.synset.obtain_synset_list",
        "bboxed-synsets": "http://www.image-net.org/api/text/imagenet.bbox.obtain_synset_list",
        "urls": "http://www.image-net.org/api/text/imagenet.synset.geturls?wnid={}",
        "mapping": "http://www.image-net.org/api/text/imagenet.synset.geturls.getmapping?wnid={}",
        # http://www.image-net.org/api/download/imagenet.bbox.synset?wnid={} is only a meta-refresh to
        "bboxes": "http://www.image-net.org/downloads/bbox/bbox/{}.tar.gz"}


# seed all random generators
if random_seed is not None:
    random.seed(random_seed)
    np.random.seed(random_seed)


def load_data(data_dir=data_path):
    """
    loads the data from `data_dir`, which should contains subdirectories containing
    images for each class
    """

    gen = ImageDataGenerator(rescale=1.0 / 255, validation_split=0.1, data_format=data_format)
    kwargs = {
            "directory": data_dir,
            "target_size": image_size,
            "class_mode": "categorical",
            "batch_size": batch_size,
            "seed": random_seed,
            "shuffle": True}

    # keep stdout clean
    with open(os.devnull, "w") as devnull, redirect_stdout(devnull):
        return (gen.flow_from_directory(subset="training", **kwargs),
                gen.flow_from_directory(subset="validation", **kwargs))
