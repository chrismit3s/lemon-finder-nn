from argparse import ArgumentParser
from os.path import join, basename
from src.dataset import all_classes, data_path, image_size, kaggle_dataset
from src.dataset.augment import random_crop, random_flip, random_rotate, resize_image
from src.helpers import int_tuple, rprint, write_images
from tempfile import TemporaryDirectory
import cv2
import kaggle.api as kl
import numpy as np
import os


def generate_images(dataset_root, class_):
    """
    generates all images out of all for `class_` relevant classes, `dataset_root`
    is a path to the downloaded kaggle dataset
    """

    # all names of the relevant classes in the kaggle dataset
    kaggle_classes = kaggle_dataset["classes"][class_]

    # all images in those relevant classes
    for path, _, filenames in os.walk(dataset_root):
        # allow repetition of names in the config to represent weights of the class
        for _ in range(kaggle_classes.count(basename(path))):
            for filename in filenames:
                yield cv2.imread(join(path, filename))


def augment_images(img_gen, size, interp, n=1):
    """
    takes a image generator `img_gen` and yields every image of it `n` times, augmented
    with [`random_crop`](/references/src/dataset/augment/#random_crop),
    [`random_rotate`](/references/src/dataset/augment/#random_rotate) and
    [`random_flip`](/references/src/dataset/augment/#random_flip), and using
    interpolation `interp`
    """

    size = np.array(size, dtype="int")

    # iterate over images
    for img in img_gen:
        # add border to get some room for random cropping
        x_border, y_border = int_tuple(size * 0.2)
        img = resize_image(img, size=size * 0.8, interp=interp)
        img = cv2.copyMakeBorder(img,
                                 top=y_border,
                                 bottom=y_border,
                                 left=x_border,
                                 right=x_border,
                                 borderType=cv2.BORDER_CONSTANT,
                                 value=(255, 255, 255))

        # augment image
        for _ in range(n):
            augmented = random_rotate(img, interp, bg_color=(255, 255, 255))  # copies img
            augmented = random_flip(augmented, horizontal=True, vertical=True)
            augmented = random_crop(augmented,
                                    size=int_tuple(size),
                                    keep_region=(int_tuple(size * 0.2), int_tuple(size)))
            yield augmented


def main(out_dir, size, interp, dataset_path=None):
    if dataset_path is None or len(dataset_path) == 0:
        # authenticate
        try:
            kl.authenticate()
        except OSError:
            raise PermissionError("Cannot authenticate, please download your API "
                                  "token from https://kaggle.com/<username>/account "
                                  "and move it to ~/.kaggle/kaggle.json")

        # create tempdir for download
        tempdir = TemporaryDirectory()
        dataset_path = tempdir.name

        # download dataset
        print("downloading dataset...", end="\r", flush=True)
        kl.dataset_download_files(kaggle_dataset["name"], path=dataset_path, unzip=True)
        print("downloading dataset - done")

    else:
        tempdir = None
        print(f"found dataset at {dataset_path.replace(os.sep, '/')}")

    # ensure outdir exists
    os.makedirs(out_dir, exist_ok=True)

    # iterate over all classes
    images_per_class = {}
    total = 0
    n_classes = len(all_classes)
    for i_class, class_ in enumerate(all_classes):
        # out_names uses os.sep.join because join thinks the ':' belongs to a drive
        # letter and treats it as an absolut path
        out_names = os.sep.join((out_dir, class_, "kaggle-{:05}.jpg"))
        os.makedirs(os.path.dirname(out_names), exist_ok=True)  # ensure path to out_names exists

        # create generator and write converted files to disk
        gen = generate_images(dataset_path, class_)
        gen = augment_images(gen, size, interp, n=kaggle_dataset["augment-factor"])
        i_image = -1  # for when gen is empty
        for i_image in write_images(gen, out_names):
            total += 1
            r = (i_class + 1 - 0.9999 ** i_image) / n_classes
            rprint(f"writing images: {100 * r:3.0f}% [{class_}]", end="\r", flush=True)
        images_per_class[class_] = i_image + 1
    rprint(f"writing images for {', '.join(all_classes)} - done - wrote {total} images")

    if tempdir is not None:
        tempdir.cleanup()

    return images_per_class


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-k", "--dataset-path",
                        dest="dataset_path",
                        help=(f"location of the downloaded kaggle dataset {kaggle_dataset['name']}, "
                              f"if not set it will download it temporarily"),
                        type=str,
                        default="")
    parser.add_argument("-o",
                        dest="out_dir",
                        help="output directory",
                        type=str,
                        default=data_path)

    parser.add_argument("--interpolation",
                        dest="interp",
                        help="interpolation used for image resizing",
                        type=str,
                        default="cubic")

    # get args as dict
    args = vars(parser.parse_args())
    main(**args, size=image_size)
