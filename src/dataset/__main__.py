from argparse import ArgumentParser
from src.dataset.kaggle import main as kaggle_main
from src.dataset.videos import main as videos_main
from src.dataset.imagenet_classes import main as imagenet_classes_main
from src.dataset.imagenet_nomatch import main as imagenet_nomatch_main
from src.dataset import all_classes, no_match_class, data_path, image_size, kaggle_dataset, video_dataset
import shutil as sh
import os


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-v", "--videos",
                        dest="vid_dir",
                        help=("input directory, containing subdirectories for "
                              "every class containing video files"),
                        type=str,
                        default=video_dataset["path"])
    parser.add_argument("-k", "--kaggle-dataset",
                        dest="kaggle_dir",
                        help=(f"location of the downloaded kaggle dataset {kaggle_dataset['name']}, "
                              f"if not set it will download it temporarily"),
                        type=str,
                        default=kaggle_dataset["path"])
    parser.add_argument("-o", "--out",
                        dest="out_dir",
                        help="output directory",
                        type=str,
                        default=data_path)

    parser.add_argument("-p", "--keep",
                        dest="keep",
                        help="skip the deletion of the old dataset, automatically turned on when -K, -I or -V are used",
                        action="store_true")

    parser.add_argument("-I", "--skip-imagenet",
                        dest="skip_imagenet",
                        help="skip the step of generating images from imagenet",
                        action="store_true")
    parser.add_argument("-V", "--skip-videos",
                        dest="skip_videos",
                        help="skip the step of generating images from videos",
                        action="store_true")
    parser.add_argument("-K", "--skip-kaggle",
                        dest="skip_kaggle",
                        help="skip the step of generating images from kaggle",
                        action="store_true")

    # parse args
    args = parser.parse_args()

    # if we skip the generation of any part of the dataset, we obviously want to keep it
    if not (args.skip_videos or args.skip_kaggle or args.skip_imagenet):
        print("Clearing old dataset:")
        print(f"deleting {args.out_dir}...", end="\r", flush=True)
        sh.rmtree(args.out_dir)
        print(f"deleting {args.out_dir} - done")
        print("")

    videos_image_counts = {}
    if args.skip_videos:
        print("Skipping videos")
        for class_ in all_classes:
            all_images = os.listdir(os.path.join(data_path, class_))
            videos_image_counts[class_] = sum(1 for img_name in all_images if img_name.startswith("videos"))
    else:
        print("Generating dataset from videos:")
        videos_image_counts = videos_main(args.vid_dir, args.out_dir, size=image_size, interp="cubic")
        print("")

    kaggle_image_counts = {}
    if args.skip_kaggle:
        print("Skipping kaggle")
        for class_ in all_classes:
            all_images = os.listdir(os.path.join(data_path, class_))
            kaggle_image_counts[class_] = sum(1 for img_name in all_images if img_name.startswith("kaggle"))
    else:
        print("Preparing kaggle dataset:")
        kaggle_image_counts = kaggle_main(args.out_dir,
                                          size=image_size,
                                          interp="cubic",
                                          dataset_path=args.kaggle_dir)
        print("")

    imagenet_image_counts = {}
    if args.skip_imagenet:
        print("Skipping imagenet")
        for class_ in all_classes:
            all_images = os.listdir(os.path.join(data_path, class_))
            imagenet_image_counts[class_] = sum(1 for img_name in all_images if img_name.startswith("imagenet"))
    else:
        print("Downloading and cropping imagenet pictures for all fruit classes:")
        imagenet_image_counts = imagenet_classes_main(args.out_dir,
                                                      size=image_size,
                                                      interp="cubic",
                                                      check_wordnet=True)
        print("Downloading and cropping imagenet pictures for the no-match classes:")
        imagenet_image_counts[no_match_class] = imagenet_nomatch_main(args.out_dir,
                                                                      size=image_size,
                                                                      interp="cubic",
                                                                      check_wordnet=False)

    image_counts = {"videos": videos_image_counts,
                    "kaggle": kaggle_image_counts,
                    "imagenet": imagenet_image_counts}
    print("Summary:")
    print("  by source:")
    for name, counts in image_counts.items():
        print(f"    {name}: {sum(counts.values())}")
    print("  by class:")
    for class_ in all_classes:
        print(f"    {class_}: {sum(counts[class_] for counts in image_counts.values())}")
    print("  table:")
    table = []
    table.append([""] + all_classes)
    for source, counts in image_counts.items():
        table.append([source] + [counts[class_] for class_ in all_classes])
    for row in table:
        print("    " + " ".join(f"{str(cell)[:8]:>8}" for cell in row))
