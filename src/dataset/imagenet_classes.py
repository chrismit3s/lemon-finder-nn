from argparse import ArgumentParser
from contextlib import redirect_stdout, suppress
from nltk.corpus import wordnet as wn
from os.path import join
from src.dataset import classes, data_format, data_path, image_size, imagenet_dataset, request_timeout
from src.dataset.augment import random_crop, random_flip, random_rotate, resize_image
from src.helpers import download_file, image_from_url, int_tuple, lookup_attr, remove_channels, rprint, write_images
from tempfile import TemporaryDirectory
from xml.etree import ElementTree as ET
import cv2
import nltk
import numpy as np
import os
import requests as rq
import tarfile


class INImage():
    """
    a class representing an imagenet image with bounding boxes
    """

    def __init__(self, url, bbox_file):
        # parse xml file
        self.tree = ET.parse(bbox_file)

        # get image and its imagenet id
        self.img_id = self.tree.find("filename")
        self.img = image_from_url(url, timeout=request_timeout)

        # check if all channels contain the same info => greyscale
        if data_format == "channels_last":
            self.is_greyscale = ((self.img[:, :, 0] == self.img[:, :, 1]).all()
                                 and (self.img[:, :, 1] == self.img[:, :, 2]).all())
        elif data_format == "channels_first":
            self.is_greyscale = ((self.img[0] == self.img[1]).all()
                                 and (self.img[1] == self.img[2]).all())

        # get size used for bboxes and actual image size to scale the bounding
        # boxes in case they differ
        obj = self.root.find("size")
        bbox_x = int(obj.find("width").text)
        bbox_y = int(obj.find("height").text)
        img_x, img_y = remove_channels(self.img.shape, data_format)
        scale_x = img_x / bbox_x
        scale_y = img_y / bbox_y

        # get all bboxes
        self.bboxes = []
        for obj in self.root.iterfind("object"):
            for bbox in obj.iterfind("bndbox"):
                xmin = int(bbox.find("xmin").text) * scale_x
                ymin = int(bbox.find("ymin").text) * scale_y
                xmax = int(bbox.find("xmax").text) * scale_x
                ymax = int(bbox.find("ymax").text) * scale_y
                self.bboxes.append(int_tuple((xmin, ymin, xmax, ymax)))

    @property
    def root(self):
        """
        property getter for the root of the xml bbox file
        """

        return self.tree.getroot()


def generate_images(url_lookup, bboxes_lookup):
    """
    generates an [`INImage`](./#image) object for every entry that is both in
    `url_lookup` and `bboxes_lookup`, which are dicts, mapping `img_id`s
    (like `"n07753592_9781"`, `"<wnid>_<i>"`) to download urls and bbox PASCAL
    VOC annotations respectively
    """

    keys = url_lookup.keys() & bboxes_lookup.keys()
    for key in keys:
        url = url_lookup[key]
        bboxes = bboxes_lookup[key]
        with suppress(ConnectionError):
            img = INImage(url, bboxes)
            if not img.is_greyscale:
                yield img


def augment_images(img_gen, size, interp, n=1):
    """
    takes a image generator `img_gen` and yields every image of it `n` times, augmented
    with [`random_crop`](/reference/src/dataset/augment/#random_crop) and
    [`random_flip`](/reference/src/dataset/augment/#random_flip), and using
    interpolation `interp`
    """

    out_w, out_h = out_size = np.array(size)

    with_border_scale = 2 * imagenet_dataset["min-crop-border"] + 1
    with_border_w, with_border_h = out_size * with_border_scale

    # lookup interpolation if its a string
    if isinstance(interp, str):
        interp = lookup_attr(cv2, "INTER_", interp, toupper=True)

    for bbimg in img_gen:
        for bbox in bbimg.bboxes:
            # copy image
            img = np.array(bbimg.img)

            # calculate width and heigth of bbox
            bbox_w = bbox[2] - bbox[0]
            bbox_h = bbox[3] - bbox[1]
            bbox_size = (bbox_w, bbox_h)

            # if the bbox leaves to little of a border for random cropping, it must
            # be rescaled so it does
            if out_w / bbox_w < with_border_scale or out_h / bbox_h < with_border_scale:
                # get max dimension of bbox and corresponding size of img along
                # that axis
                out_max, bbox_max = max(zip(out_size, bbox_size), key=lambda x: x[1])
                scale = out_max / (bbox_max * with_border_scale)
                img = resize_image(img, scale=scale, interp=interp)

                # recalculate bbox
                bbox = int_tuple(x * scale for x in bbox)

            # check if rescaled image is even big enough to contain the output size
            img_w, img_h = remove_channels(img.shape, data_format)
            if out_w > img_w or out_h > img_h:
                continue

            # augment image
            for _ in range(n):
                augmented = random_crop(img, size, keep_region=bbox, copy=True)
                augmented = random_rotate(augmented, interp, bg_color=None)  # copies img
                augmented = random_flip(augmented, horizontal=True)
                yield augmented


def ensure_wordnet():
    try:
        wn.synsets("")
        print("found existing corpora/wordnet directory")
    except LookupError:
        print("downloading corpora/wordnet...", end="\r", flush=True)
        with open(os.devnull, "w") as devnull, redirect_stdout(devnull):
            nltk.download("wordnet")
        print("downloading corpora/wordnet - done")


def main(out_dir, size, interp, check_wordnet=True):
    # ensure wordnet resource is loaded
    if check_wordnet:
        ensure_wordnet()

    # get all possible synsets with bboxes
    bboxed_wnids = set(rq.get(imagenet_dataset["bboxed-synsets"]).text.split("\n"))

    # iterate over all classes
    images_per_class = {}
    total = 0
    n_classes = len(classes)
    for i_class, class_ in enumerate(classes):
        with TemporaryDirectory() as tempdir:
            # out_names uses os.sep.join because join thinks the ':' belongs to a drive
            # letter and treats it as an absolut path
            out_names = os.sep.join((out_dir, class_, "imagenet-{:05}.jpg"))
            os.makedirs(os.path.dirname(out_names), exist_ok=True)  # ensure path to out_names exists

            # use the first synset which also has bbox information
            wnids = list(filter(lambda wnid: wnid in bboxed_wnids,
                                ("n{:08}".format(s.offset()) for s in wn.synsets(class_))))
            if len(wnids) == 0:
                raise ValueError(f"No synset found for class {class_}")
            wnid = wnids[0]

            # download .tar.gz file for images with bounding boxes, so we only use
            # those and extract .xml files
            bboxes_archive = download_file(imagenet_dataset["bboxes"].format(wnid),
                                           join(tempdir, "bboxes.tar.gz"))
            with tarfile.open(bboxes_archive) as f:
                f.extractall(path=tempdir)  # extracts the only member 'Annotation' in this archive

            # create a lookup from img_id to bbox file
            bboxes_path = join(tempdir, "Annotation", wnid)
            bboxes_lookup = dict((filename[:-4], join(bboxes_path, filename))
                                 for filename in os.listdir(bboxes_path))

            # get mapping from img_id to download url
            r = rq.get(imagenet_dataset["mapping"].format(wnid))
            url_lookup = dict(line.strip().split(" ") for line in r.text.split("\n") if " " in line)

            # get generator for all files with bounding boxes
            gen = generate_images(url_lookup, bboxes_lookup)
            gen = augment_images(gen, size, interp, n=imagenet_dataset["augment-factor"])
            for i_image in write_images(gen, out_names):
                total += 1
                r = (i_class + 1 - 0.997 ** i_image) / n_classes
                rprint(f"writing images: {100 * r:3.0f}% [{class_}]", end="\r", flush=True)
            images_per_class[class_] = i_image + 1
    rprint(f"writing images for {', '.join(classes)} - done - wrote {total} images")
    return images_per_class


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-o",
                        dest="out_dir",
                        help="output directory",
                        type=str,
                        default=data_path)

    parser.add_argument("--interpolation",
                        dest="interp",
                        help="interpolation used for image resizing",
                        type=str,
                        default="cubic")

    # get args as dict
    args = vars(parser.parse_args())
    main(**args, size=image_size)
