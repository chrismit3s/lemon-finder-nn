from argparse import ArgumentParser
from nltk.corpus import wordnet as wn
from os.path import join
from random import choice, sample
from concurrent.futures import ThreadPoolExecutor
from concurrent import futures as ft
from src.dataset import classes, data_path, image_size, imagenet_dataset, max_request_retries, no_match_class, request_timeout  # noqa:E501
from src.dataset.augment import random_crop, random_flip, random_resize
from src.dataset.imagenet_classes import ensure_wordnet
from src.helpers import image_from_url, rprint
import cv2
import os
import requests as rq


def generate_urls(n, wnids, repeat_wnid=100):
    """
    generates `n` imagenet urls, picked from any of the wnids in `wnids`, a new
    wnid is only picked after `repeat_wnid` images for performace reasons
    """

    # generate urls
    while n > 0:
        wnid = choice(wnids)

        r = rq.get(imagenet_dataset["mapping"].format(wnid))
        lines = set(line.strip() for line in r.text.split("\n"))
        wnid_urls = [line.split(" ")[-1] for line in lines if " " in line]

        chosen_urls = sample(wnid_urls, repeat_wnid) if repeat_wnid <= len(wnid_urls) else wnid_urls
        yield from chosen_urls[:n]  # slice so that we never yield too many urls
        n -= len(chosen_urls)


def augmented_images(url, size, interp, n=1):
    """
    returns a list of `n` images downloaded from `url` and augmented
    with [`random_crop`](/reference/src/dataset/augment/#random_crop),
    [`random_resize`](/reference/src/dataset/augment/#random_resize) and
    [`random_flip`](/reference/src/dataset/augment/#random_flip), and using
    interpolation `interp`
    """

    try:
        img = image_from_url(url, max_retries=max_request_retries, timeout=request_timeout)
    except ConnectionError:
        return []

    # augment image
    ret = []
    for _ in range(n):
        augmented = random_resize(img, min_size=size)
        augmented = random_flip(augmented, horizontal=True)
        augmented = random_crop(augmented, size)
        ret.append(augmented)
    return ret


def main(out_dir, size, interp, check_wordnet=True):
    # ensure wordnet resource is loaded
    if check_wordnet:
        ensure_wordnet()

    # generate possible wnids for no match class, only use bboxed synsets as those are
    # more readily available and have higher connection speeds
    possible_wnids = set(rq.get(imagenet_dataset["bboxed-synsets"]).text.split("\n"))

    # exclude any wnid which is fits any actual class
    for class_ in classes:
        possible_wnids -= set("n{:08}".format(s.offset()) for s in wn.synsets(class_))
    possible_wnids = list(possible_wnids)

    # nomatch should have about 20% more images, to help with convergence
    images_per_class = (len(os.listdir(join(data_path, class_))) for class_ in classes)
    images_in_no_match_class = len(os.listdir(join(data_path, no_match_class)))
    n = max(images_per_class) - images_in_no_match_class

    # no match class already has enough images
    if n < 0:
        return 0

    # out_names uses os.sep.join because join thinks the ':' belongs to start drive
    # letter and treats it as an absolut path
    out_names = os.sep.join((out_dir, no_match_class, "imagenet-{:05}.jpg"))

    rprint("starting thread pool...", end="\r", flush=True)
    with ThreadPoolExecutor() as pool:
        rprint("starting thread pool - done")

        n_urls = n // imagenet_dataset["augment-factor"]
        futures = []
        for i_url, url in enumerate(generate_urls(n_urls, possible_wnids)):
            r = i_url / n_urls
            rprint(f"generating urls: {100 * r:3.0f}%", end="\r", flush=True)
            futures.append(pool.submit(augmented_images, url, size, interp, n=imagenet_dataset["augment-factor"]))
        rprint(f"generating urls - done - created {n_urls} tasks")

        total = 0
        for i_url, images_future in enumerate(ft.as_completed(futures)):
            imgs = images_future.result()
            n_imgs = len(imgs)

            for i_img, img in enumerate(imgs):
                r = (i_url + i_img / n_imgs) / n_urls
                rprint(f"writing images: {100 * r:3.0f}%", end="\r", flush=True)

                cv2.imwrite(out_names.format(total), img)
                total += 1
        rprint(f"writing images for {no_match_class} - done - wrote {total} images")

    return total


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-o",
                        dest="out_dir",
                        help="output directory",
                        type=str,
                        default=data_path)

    parser.add_argument("--interpolation",
                        dest="interp",
                        help="interpolation used for image resizing",
                        type=str,
                        default="cubic")

    # get args as dict
    args = vars(parser.parse_args())
    main(**args, size=image_size)
