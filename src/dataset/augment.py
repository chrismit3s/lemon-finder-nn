from src.dataset import data_format
from src.helpers import lookup_attr, int_tuple, remove_channels
from random import random, uniform
import cv2
import numpy as np


def crop_image(img, region, copy=False):
    """
    crops an image `img` to a region `region`, which must be either of shape
    `(x1, y1, x2, y2)` or `((x1, y1), (x2, y2))`, with `(x1, y1)` being the top
    left corner (inclusive) and `(x2, y2)` being the bottom right corner
    (exclusive), if `copy` is set to `True` (defaults to `False`), the the
    resulting cutout will be a new numpy array, else it will just be a view of
    the old one
    """

    # unpack region
    if len(region) == 2:
        ((x1, y1), (x2, y2)) = region
    elif len(region) == 4:
        (x1, y1, x2, y2) = region
    else:
        raise ValueError(f"Unknown format for region: {region}")

    # check bounds
    xmax, ymax = remove_channels(img.shape, data_format)

    if (x1 not in range(xmax) or x2 not in range(xmax + 1)
            or y1 not in range(ymax) or y2 not in range(ymax + 1)):
        raise IndexError(f"Index out of bounds for image of size {(xmax, ymax)} "
                         f"(width, height) and region from {(x1, y1)} to {(x2, y2)}")

    res = img[y1:y2, x1:x2]  # again... opencv's fault
    return np.array(res) if copy else res


def ratiocrop_image(img, ratio):
    """
    takes an image `img` and center-crops it to an aspect ratio of `ratio`
    (= `width / height`), or some close approximation
    """
    width, height = remove_channels(img.shape, data_format)

    # calculate new width, assuming we can use the full img height
    new_width = ratio * height
    if new_width <= width:
        # we _can_ use the full height
        new_height = height
    else:
        # we _can't_ use the full height, so we must use the full img width
        new_width = width
        new_height = width / ratio

    # go from the center of the img _half_ of the new dimensions in _both_ directions
    w_center = int(width / 2)  # width center
    h_center = int(height / 2)  # height center
    w_offset = int(new_width / 2)  # width offset
    h_offset = int(new_height / 2)  # height offset
    return img[h_center - h_offset:h_center + h_offset, w_center - w_offset:w_center + w_offset]


def rotate_image(img, angle, interp, bg_color=0):
    """
    rotates an image `img` counter-clockwise `angle` degrees using `interp` to
    interpolate inbetween pixels and without changing its dimensions, so some
    pixels may be cut off, and outliers are filled in with `bg_color` or, if it's
    not set, filled with the border color according to
    [`cv2.BORDER_REPLICATE`](https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html),
    always copies the input
    """

    # calculate rotation matrix
    xmax, ymax = remove_channels(img.shape, data_format)
    center = (xmax / 2, ymax / 2)
    rotation_matrix = cv2.getRotationMatrix2D(center[::-1], angle, scale=1.0)

    # lookup interpolation if its a string
    if isinstance(interp, str):
        interp = lookup_attr(cv2, "INTER_", interp, toupper=True)

    # rotate image
    rotated = cv2.warpAffine(img, rotation_matrix,
                             dsize=(ymax, xmax),
                             flags=interp,
                             borderMode=cv2.BORDER_REPLICATE if bg_color is None else cv2.BORDER_CONSTANT,
                             borderValue=bg_color)

    return rotated


def resize_image(img, scale=None, size=None, interp="linear"):
    """
    resizes an image `img` by a factor of `scale` if `size` is `None`, so the
    resulting image will have a size of `round(img.size * scale)`; resizes to a
    size of `size` if `scale` is `None`; _exaclty_ one of `scale` or `size` _must_
    be `None`, always copies the input
    """

    # check if _exactly_ one of scale, size is None
    if (scale is None) == (size is None):
        raise ValueError(f"*Exactly* one of size and scale must be None "
                         f"({size=}, {scale=})")

    # lookup interpolation if its a string
    if isinstance(interp, str):
        interp = lookup_attr(cv2, "INTER_", interp, toupper=True)

    return cv2.resize(img,
                      dsize=(0, 0) if size is None else int_tuple(size),
                      fx=scale or 0, fy=scale or 0,
                      interpolation=interp)


def flip_image(img, horizontal=False, vertical=False, copy=False):
    """
    flips an image `img` vertically (up - down) if `vertical` is `True` (defaults
    to `False`) and horizontally (left - right) if `horizontal` is `True` (defaults
    to `False`), if `copy` is set to `True` (defaults to `False`), the the
    resulting cutout will be a new numpy array, else it will just be a view of
    the old one
    """

    x = -1 if horizontal else 1
    y = -1 if vertical else 1

    res = img[::y, ::x]  # x and y are swapped because of opencv *shruggie*
    return np.array(res) if copy else res


def random_crop(img, size, keep_region=None, copy=False):
    """
    randomly crops an image `img` to `size`, while guaranteeing that the region
    `keep_region` (in a format like in [`crop_image`](./#crop_image), or `None`
    for a completely random crop) will always be visible, if `copy` is set to
    `True` (defaults to `False`), the the resulting cutout will be a new numpy
    array, else it will just be a view of the old one
    """

    img_size = np.array(remove_channels(img.shape, data_format))
    size = np.array(size)

    # unpack keep_region
    if keep_region is None:
        from_x = from_y = to_x = to_y = 0
    elif len(keep_region) == 2:
        ((from_x, from_y), (to_x, to_y)) = keep_region
    elif len(keep_region) == 4:
        (from_x, from_y, to_x, to_y) = keep_region
    else:
        raise ValueError(f"Unknown format for keep_region: {keep_region}")
    from_ = np.array((from_x, from_y), dtype="int")
    to = np.array((to_x, to_y), dtype="int")

    # start is the top-left most point the cropped image _could_ include, and
    # end is the top-left most point the cropped image _can't_ include
    start = np.max([(0, 0), to - size], axis=0)
    end = np.min([img_size, from_ + size], axis=0)

    try:
        # generate random point from start (top-left most point), and end - size
        # (bottom-right-most point), +1 as randint's range is [a, b), and
        topleft = np.random.randint(start, end - size + 1)
    except ValueError:  # empty range supplied to randint
        raise IndexError(f"Region keep_region={(from_x, from_y, to_x, to_y)} can't "
                         f"fit in a image of size={img_size} or in a cropped image "
                         f"of {size=} ({start=}, {end=})") from None

    # return cropped image
    region = int_tuple((*topleft, *(topleft + size)))
    return crop_image(img, region, copy)


def random_rotate(img, interp, bg_color=None):
    """
    randomly rotates an image `img` using `interp` to interpolate inbetween
    pixels and without changing its dimensions, so some pixels may be cut off,
    and outliers are filled in with `bg_color` or, if it's not set, filled with
    the border color according to
    [`cv2.BORDER_REPLICATE`](https://docs.opencv.org/2.4/modules/imgproc/doc/filtering.html),
    always copies the input
    """
    return rotate_image(img, angle=uniform(0, 360), interp=interp, bg_color=bg_color)


def random_resize(img, min_size=None, max_size=None, interp="linear"):
    """
    randomly resizes an image `img` to a size between `min_size` and `max_size`,
    if `min_size` is `None`, `min_size` is assumed to be `(1, 1)`, if `max_size`
    is `None`, `max_size` is assumed to be `img.size`; always copies the input
    """

    img_size = np.array(remove_channels(img.shape, data_format))

    # fill unknown bounds
    min_size = np.array((1, 1) if min_size is None else min_size)
    max_size = np.array(img_size if max_size is None else max_size)

    # calculate the range in which we must pick the actual scale
    min_scale = np.max(min_size / img_size)
    max_scale = np.min(max_size / img_size)

    if min_scale > max_scale:
        max_scale = min_scale
    return resize_image(img, scale=uniform(min_scale, max_scale), interp=interp)


def random_flip(img, horizontal=False, vertical=False, copy=False):
    """
    randomly flips an image `img` maybe vertically (up - down) if `vertical`
    is `True` (defaults to `False`) and maybe horizontally (left - right) if
    `horizontal` is `True` (defaults to `False`) or maybe neither, if `copy` is
    set to `True` (defaults to `False`), the the resulting cutout will be a new
    numpy array, else it will just be a view of the old one
    """
    return flip_image(img,
                      horizontal=horizontal and (random() < 0.5),
                      vertical=vertical and (random() < 0.5),
                      copy=copy)
