from argparse import ArgumentParser
from fractions import Fraction
from os.path import join
from src.dataset.augment import random_crop, random_flip, random_rotate, ratiocrop_image, resize_image
from src.dataset import all_classes, classes, data_path, image_size, no_match_class, video_dataset
from src.helpers import int_tuple, lookup_attr, rprint, write_images
import numpy as np
import cv2
import os


class Video():
    """
    a class representing a video file on disk
    """

    def __init__(self, filename):
        self.filename = filename
        self.cap = None
        self.__enter__()

    def __len__(self):
        return int(self.frame_count)

    def __enter__(self, *args, **kwargs):
        if self.cap is not None:
            self.__exit__()
        self.cap = cv2.VideoCapture(self.filename)
        return self

    def __exit__(self, *args, **kwargs):
        self.cap.release()
        self.cap = None

    def __getattr__(self, attr):
        value = lookup_attr(cv2, "CAP_PROP_", attr, toupper=True, forward_error=True)
        return self.cap.get(value)

    @property
    def width(self):
        return int(self.image_width)

    @property
    def height(self):
        return int(self.image_height)

    @property
    def resolution(self):
        return (self.width, self.height)

    def frames(self):
        ret = True
        while ret:
            ret, image = self.cap.read()
            if image is not None:
                yield image


def generate_images(videos):
    """
    yields every image from the videos in `videos` which must be an iterable of
    `Video` Objects
    """

    for video in videos:
        with video:
            yield from video.frames()


def skip_images(img_gen, n=2):
    """
    skips 1 out of `n` images of `img_gen`, `n` can also be a float, but must be
    bigger than 1, the resulting generator will have a length of `(1 - 1 / n) * len(img_gen)`,
    for example `n = 2.5` would skip 4 out of 10 images
    """

    k, j = Fraction(n).limit_denominator(100).as_integer_ratio()

    for i, img in enumerate(img_gen):
        if i % k >= j:
            yield img


def augment_images(img_gen, size, interp, n=1):
    """
    takes a image generator `img_gen` and yields every image of it `n` times, augmented
    with [`random_crop`](/references/src/dataset/augment/#random_crop) and
    [`random_flip`](/references/src/dataset/augment/#random_flip), and using
    interpolation `interp`
    """

    ratio = size[0] / size[1]
    size = np.array(size, dtype="int")
    min_border = video_dataset["min-crop-border"]

    # iterate over images
    for img in img_gen:
        # fix image ratio and resolution
        img = ratiocrop_image(img, ratio)
        img = resize_image(img, size=size * (2 * min_border + 1), interp=interp)

        # augment image
        for _ in range(n):
            augmented = random_rotate(img, interp, bg_color=None)  # copies img
            augmented = random_flip(augmented, horizontal=True, vertical=True)
            augmented = random_crop(augmented,
                                    size=int_tuple(size),
                                    keep_region=(int_tuple(size * min_border), int_tuple(size)))
            yield augmented


def main(vid_dir, out_dir, size, interp):
    # ensure outdir exists
    os.makedirs(out_dir, exist_ok=True)

    # iterate over all classes
    images_per_class = {}
    total = 0
    n_classes = len(all_classes)
    for i_class, class_ in enumerate(all_classes):
        # out_names uses os.sep.join because join thinks the ':' belongs to a drive
        # letter and treats it as an absolut path
        out_names = os.sep.join((out_dir, class_, "videos-{:05}.jpg"))
        os.makedirs(os.path.dirname(out_names), exist_ok=True)  # ensure path to out_names exists

        vid_path = join(vid_dir, class_)
        videos = [Video(join(vid_path, filename))
                  for filename in os.listdir(vid_path)]

        # total numer of images and number of digits in that number
        n_images = (
                sum(len(video) for video in videos)
                * video_dataset["augment-factor"]
                * (1 - 1 / video_dataset["frame-skip"]))

        # create generator and write converted files to disk
        gen = generate_images(videos)
        gen = skip_images(gen, n=video_dataset["frame-skip"])
        gen = augment_images(gen, size, interp, n=video_dataset["augment-factor"])
        for i_image in write_images(gen, out_names):
            total += 1
            r = (i_class + i_image / n_images) / n_classes
            rprint(f"extracting frames: {100 * r:3.0f}% [{class_}]", end="\r", flush=True)
        images_per_class[class_] = i_image + 1
    rprint(f"extracting frames for {', '.join(all_classes)} - done - wrote {total} images")
    return images_per_class


if __name__ == "__main__":
    parser = ArgumentParser()

    parser.add_argument("-v",
                        dest="vid_dir",
                        help=("input directory, containing subdirectories for "
                              "every class containing video files"),
                        type=str,
                        default=video_dataset["path"])
    parser.add_argument("-o",
                        dest="out_dir",
                        help="output directory",
                        type=str,
                        default=data_path)

    parser.add_argument("--interpolation",
                        dest="interp",
                        help="interpolation used for image resizing",
                        type=str,
                        default="cubic")

    # get args as dict
    args = vars(parser.parse_args())
    main(**args, size=image_size)
